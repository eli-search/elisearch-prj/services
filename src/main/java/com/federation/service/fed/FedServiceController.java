package com.federation.service.opensearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FedServiceController {

    @RestController
public class FederationServiceController {
	private static final String sparqlResponseXml = "<?xml version=\"1.0\"?>" +
			"<sparql xmlns=\"http://www.w3.org/2005/sparql-results#\">" +
			" <head>" +
			"   <variable name=\"exp\"/>" +
			" </head>" +
			" <results>" +
			"   <result>" +
			"     <binding name=\"exp\"><uri>http://data.legilux.public.lu/eli/etat/leg/rgd/2022/03/09/a97/jo</uri></binding>" +
			"   </result>" +
			" </results>" +
			"</sparql>";


	private static final String sparqlResponseJson = "{" +
  		"\"head\": { \"vars\": [ \"exp\" ]" +
		"} ," +
		"	\"results\": {" +
		"\"bindings\": [" +
		"{" +
		"	\"exp\": { \"type\": \"uri\" , \"value\": \"http://data.legilux.public.lu/eli/etat/leg/rgd/2022/03/09/a97/jo\" }" +
		"}" +
    		"]"+
	"}"+
	"}";

  
	@PostMapping("/sparql")
	public ResponseEntity processRequest(@RequestHeader("Accept") String accept) {
		HttpHeaders responseHeaders = new HttpHeaders();
		String sparqlResponse = "";
    
    System.out.println("ACCEPTE VALUE:" + accept);
		if (accept.equals("application/sparql-results+json")) {
      responseHeaders.setContentType(MediaType.APPLICATION_JSON);
      sparqlResponse = sparqlResponseJson;
		} else {
			responseHeaders.setContentType(MediaType.APPLICATION_XML);
      sparqlResponse = sparqlResponseXml;
		}
			return new ResponseEntity<>(sparqlResponse, responseHeaders, HttpStatus.OK);
  }
}

}
