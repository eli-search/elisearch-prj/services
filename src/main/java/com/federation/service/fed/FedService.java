package com.federation.service.opensearch;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException; 

public class FedService {

    public void createQuery() {
      //Creating a JSONObject object
      JSONObject jsonObject = new JSONObject();

	  //Creating a JSONObject object
	  JSONObject jsonObjectBool = new JSONObject();
	  jsonObjectBool.put("bool", "1");

	  JSONArray jsonObjectFields = new JSONArray();
	  jsonObjectFields.add("title_x_en");
	  jsonObjectFields.add("description_x_en");

	  //Creating a JSONObject object
	  JSONObject jsonObjectMultiMatch = new JSONObject();
	  jsonObjectMultiMatch.put("query", "car");
	  jsonObjectMultiMatch.put("type", "best_fields");
	  jsonObjectMultiMatch.put("fields", jsonObjectFields);
	   
	  JSONArray jsonObjectArray = new JSONArray();
	  jsonObjectArray.add(jsonObjectMultiMatch);

	  //Creating a JSONObject object
	  JSONObject jsonObjectShould = new JSONObject();
	  jsonObjectShould.put("should", jsonObjectArray);

	  //Inserting key-value pairs into the json object
	  jsonObjectBool.put("bool", jsonObjectShould);

      //Inserting key-value pairs into the json object
      jsonObject.put("query", jsonObjectBool);

      try {
         FileWriter file = new FileWriter("/home/nobrean_linux/workstation/ELI-SEARCH/FederationService/output.json");
         file.write(jsonObject.toJSONString());
         file.close();
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      System.out.println("JSON file created: "+jsonObject);
    }

	public String readJSONValues(String jsonString){

		String results = null;
		try{
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(jsonString);

		// Parse the JSON string
		JSONObject jsonObject = new JSONObject(json);

		// Access the nested objects
		JSONObject spqrqlObject = (JSONObject) jsonObject.get("sparql");
		JSONObject resultsbject = (JSONObject) spqrqlObject.get("results");
		JSONObject queryresult = (JSONObject) resultsbject.get("result");
		JSONObject queryBinding = (JSONObject) queryresult.get("binding");
		results = (String) queryBinding.get("uri");

		System.out.println("Query: " + results);
		} catch(ParseException e){
			System.out.println("ParseException: " + e);
		}

        return results;
	}
	
	public static void main(String[] args) {
		FedService opensearch = new FedService();
		opensearch.createQuery();
		//opensearch.readJSON();
	}

	public static String responseFromOpenSearch(){
		String response = "{\n" + //
				"  \"head\": {\n" + //
				"    \"link\": [],\n" + //
				"    \"vars\": [\n" + //
				"      \"exp\",\n" + //
				"      \"p\",\n" + //
				"      \"o\"\n" + //
				"    ]\n" + //
				"  },\n" + //
				"  \"results\": {\n" + //
				"    \"distinct\": false,\n" + //
				"    \"ordered\": true,\n" + //
				"    \"bindings\": [\n" + //
				"      {\n" + //
				"        \"exp\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#DefaultQuadMap\"\n" + //
				"        },\n" + //
				"        \"p\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"\n" + //
				"        },\n" + //
				"        \"o\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#QuadMap\"\n" + //
				"        }\n" + //
				"      },\n" + //
				"      {\n" + //
				"        \"exp\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#TmpQuadMap\"\n" + //
				"        },\n" + //
				"        \"p\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"\n" + //
				"        },\n" + //
				"        \"o\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#QuadMap\"\n" + //
				"        }\n" + //
				"      },\n" + //
				"      {\n" + //
				"        \"exp\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#DefaultServiceMap\"\n" + //
				"        },\n" + //
				"        \"p\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"\n" + //
				"        },\n" + //
				"        \"o\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#QuadMap\"\n" + //
				"        }\n" + //
				"      },\n" + //
				"      {\n" + //
				"        \"exp\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/virtrdf-data-formats#default-iid\"\n" + //
				"        },\n" + //
				"        \"p\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"\n" + //
				"        },\n" + //
				"        \"o\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#QuadMapFormat\"\n" + //
				"        }\n" + //
				"      },\n" + //
				"      {\n" + //
				"        \"exp\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/virtrdf-data-formats#default-iid-nullable\"\n" + //
				"        },\n" + //
				"        \"p\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"\n" + //
				"        },\n" + //
				"        \"o\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#QuadMapFormat\"\n" + //
				"        }\n" + //
				"      },\n" + //
				"      {\n" + //
				"        \"exp\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/virtrdf-data-formats#default-iid-blank\"\n" + //
				"        },\n" + //
				"        \"p\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"\n" + //
				"        },\n" + //
				"        \"o\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#QuadMapFormat\"\n" + //
				"        }\n" + //
				"      },\n" + //
				"      {\n" + //
				"        \"exp\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/virtrdf-data-formats#default-iid-blank-nullable\"\n" + //
				"        },\n" + //
				"        \"p\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"\n" + //
				"        },\n" + //
				"        \"o\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#QuadMapFormat\"\n" + //
				"        }\n" + //
				"      },\n" + //
				"      {\n" + //
				"        \"exp\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/virtrdf-data-formats#default-iid-nonblank\"\n" + //
				"        },\n" + //
				"        \"p\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"\n" + //
				"        },\n" + //
				"        \"o\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#QuadMapFormat\"\n" + //
				"        }\n" + //
				"      },\n" + //
				"      {\n" + //
				"        \"exp\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/virtrdf-data-formats#default-iid-nonblank-nullable\"\n" + //
				"        },\n" + //
				"        \"p\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"\n" + //
				"        },\n" + //
				"        \"o\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#QuadMapFormat\"\n" + //
				"        }\n" + //
				"      },\n" + //
				"      {\n" + //
				"        \"exp\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/virtrdf-data-formats#default\"\n" + //
				"        },\n" + //
				"        \"p\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"\n" + //
				"        },\n" + //
				"        \"o\": {\n" + //
				"          \"type\": \"uri\",\n" + //
				"          \"value\": \"http://www.openlinksw.com/schemas/virtrdf#QuadMapFormat\"\n" + //
				"        }\n" + //
				"      }\n" + //
				"    ]\n" + //
				"  }\n" + //
				"}";

return response;
}
		
}
