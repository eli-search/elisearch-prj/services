package com.federation.service.opensearch;

import java.util.List;
import java.util.Map;

public class SparqlResponse {

    private List<Map<String, String>> results;
  
    public List<Map<String, String>> getResults() {
      return results;
    }
  
    public void setResults(List<Map<String, String>> results) {
      this.results = results;
    }
  }
