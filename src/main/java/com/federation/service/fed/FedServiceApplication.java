package com.federation.service.opensearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FedServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FedServiceApplication.class, args);
	}

}
