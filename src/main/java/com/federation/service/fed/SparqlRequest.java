package com.federation.service.opensearch;

public class SparqlRequest {

    private String sparqlQuery;
  
    public String getSparqlQuery() {
      return sparqlQuery;
    }
  
    public void setSparqlQuery(String sparqlQuery) {
      this.sparqlQuery = sparqlQuery;
    }
  }
